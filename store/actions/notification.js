// action requestors

export const getNotification = (...args) => ({
  type: 'app/getNotification',
  args
})
export const clearNotificationState = (...args) => ({
  type: 'app/clearNotificationState',
  args
})
export const setNotificationState = (notification)=>({
    type: 'app/setNotificationState',
    payload: notification
})

export const getNotificationFromApi = (...args) =>({
    type: 'app/getNotificationFromApi',
    args
})

export const setNotificationStateFromApi = (notification)=>({
    type: 'app/setNotificationStateFromApi',
    payload: notification
})
import { takeLatest, takeEvery } from 'redux-saga/effects'

import api from '~/store/api'
import { createRequestSaga } from '~/store/sagas/common'
import {setNotificationState, setNotificationStateFromApi} from '~/store/actions/notification'
import { setToast, noop, forwardTo } from '~/store/actions/common'

const requestGetNotificationAsync = createRequestSaga({
    request: api.notification.getNotification,
    key: 'getNotification',    
    success: [
        (data) => {
            console.log('CALL SUCCESS')
            return setNotificationState(data)
        }      
    ],
    failure: [
        () => setToast('Load notification fail!', 'error'),

    ],
})

const requestGetNotificationFromApiAsync = createRequestSaga({
    request: api.notification.getNotificationFromApi,
    key: 'getNotificationFromApi',
    success: [
        (response)=>{
            console.log('Noti Response', response)
            return setNotificationStateFromApi(response.updated.notifyResponse.lstNotification)
        }
    ],
    failure: [
        (response) => setToast('Get Notification Failed: '+JSON.stringify(response), 'error')
    ]
})

// root saga reducer
export default [
    // like case return, this is take => call
    // inner function we use yield*
    // from direct watcher we just yield value
    function* asyncNotificationFetchWatchers() {
        // use takeLatest instead of take every, so double click in short time will not trigger more fork
        yield [
            takeLatest('app/getNotification', requestGetNotificationAsync),            
        ]
    },
    function* asyncNotificationApiFetchWatchers(){
        yield [
            takeLatest('app/getNotificationFromApi', requestGetNotificationFromApiAsync)
        ]
    }
]



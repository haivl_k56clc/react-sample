import { fetchJson, fetchJsonWithToken, apiPost } from '~/store/api/common'
import {
  CLINGME_SERVER
} from '~/store/constants/api'
import md5 from 'md5'
export default {
  /**
  * Logs a user in, returning a promise with `true` when done
  * @param  {string} token The token of the user  
  */
  loginFacebook(accessToken) {
    // Post request to server
    return fetchJson(`/oauth/facebook/token?access_token=${accessToken}`, {
      method: 'POST',
    })
  },

  loginGoogle(accessToken) {
    // Post request to server
    return fetchJson(`/oauth/google/token?access_token=${accessToken}`, {
      method: 'POST',
    })
  },

  login(username, password) {
    return apiPost(`/token`, {
      username,
      password,
      grant_type: 'password',
    })
  },

  loginClingme(username, password) {
    return fetch(CLINGME_SERVER + 'login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-VERSION': 1,
        'X-TIMESTAMP': Math.floor((new Date().getTime()) / 1000),
        'X-DATA-VERSION': 1,
        'X-AUTH': ''
      },
      body: JSON.stringify({
        userName: username,
        password: md5(password),
      })
    }).then(async(response)=>{
      const xsession = response.headers.map['x-session'][0]
      const body = await response.json() 
      return {...body.updated.account, xsession}
    })
    // Dummy solution
    
    // .then(response=>{
    //   return Promise.all(
    //     [
    //       response.json(),
    //       new Promise((resolve, reject)=>{
    //           resolve(response.headers)
    //       })
    //     ]
    //   )
    // }).then(responseJSON=>{
    //   return {
    //       ...,
    //       xsession: responseJSON[1].map['x-session'][0]
    //   }
    // })
  },
  refreshAccessToken(refreshToken) {
    return fetchJson(`/auth/token`, {
      method: 'POST',
      body: JSON.stringify({ refreshToken })
    })
  },

  reject(refreshToken) {
    return fetchJson(`/auth/reject`, {
      method: 'POST',
      body: JSON.stringify({ refreshToken })
    })
  },

  /**
  * Logs the current user out
  */
  logout(accessToken) {
    // return fetchJsonWithToken(token, `/logout`)
    return apiPost(`/api/Account/Logout`, {}, accessToken)
  },

}

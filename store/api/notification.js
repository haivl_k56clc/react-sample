import { apiCall, apiPost, fetchJson } from '~/store/api/common'
import { NOTIFICATION_TYPE } from '~/store/constants/app'
import {
    CLINGME_SERVER
} from '~/store/constants/api'
export default {
    getNotificationFromApi(page = 1, xsession) {
       return fetch(CLINGME_SERVER + 'notify/list?page=' + page, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-VERSION': 1,
                'X-TIMESTAMP': Math.floor((new Date().getTime()) / 1000),
                'X-DATA-VERSION': 1,
                'X-AUTH': '',
                'X-SESSION': xsession
            }
        }).then(async(response)=>{
            const responseBody = await response.json()
            if (responseBody.code){
                return Promise.reject(responseBody)
            }
            return responseBody
        })
    },
    getNotification() {
        const ret = [
            {
                type: NOTIFICATION_TYPE.COMMENT,
                user: 'Trang Lê',
                placeName: 'Bác Tôm',
                place: '21 Trung Kính, Cầu giấy, Hà Nội',
                date: '21/03/2017'
            },
            {
                type: NOTIFICATION_TYPE.REPLY_COMMENT,
                user: 'Trang Lê',
                author: 'Trà Nguyễn',
                place: '21 Trung Kính, Cầu giấy, Hà Nội',
                date: '21/03/2017'
            },
            {
                type: NOTIFICATION_TYPE.TRANSACTION_OFFLINE,
                user: 'Trang Lê',
                place: '21 Trung Kính, Cầu giấy, Hà Nội',
                date: '21/03/2017'
            },
            {
                type: NOTIFICATION_TYPE.TRANSACTION_ONLINE,
                user: 'Trang Lê',
                place: '21 Trung Kính, Cầu giấy, Hà Nội',
                date: '21/03/2017'
            },
            {
                type: NOTIFICATION_TYPE.REMINDER_EXPIRED,
                expireInterval: 1,
                placeName: 'Ăn sạch, sống khỏe với Bác Tôm',
                place: '21 Trung Kính, Cầu giấy, Hà Nội',
                date: '21/03/2017'
            },
            {
                type: NOTIFICATION_TYPE.REMINDER_WILL_EXPIRED,
                expireInterval: 3,
                placeName: 'Ăn sạch, sống khỏe với Bác Tôm',
                place: '21 Trung Kính, Cầu giấy, Hà Nội',
                date: '21/03/2017'
            },
            {
                type: NOTIFICATION_TYPE.SYSTEM_SUPPORT,
                content: 'Chương trình hỗ trợ khách hàng tháng 03/2017',
                date: '8/3/2019'
            },
            {
                type: NOTIFICATION_TYPE.SYSTEM_DEBT,
                content: 'Thông báo công nợ tháng 03/2017',
                date: '8/3/2019'
            }
        ];

        return new Promise((resolve, reject) => {
            setTimeout(() => resolve(ret), 7000)
        })
    }
}
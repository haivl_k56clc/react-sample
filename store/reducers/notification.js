export const notificationReducer = (state = null, { type, payload }) => {
    switch (type) {
        case 'app/setNotificationState':
            return { ...state, ...payload }
        case 'app/setNotificationStateFromApi':
            return {...state, ...payload }
        case 'app/clearNotificationState':
            return []
        default: 
            return state
    }
}
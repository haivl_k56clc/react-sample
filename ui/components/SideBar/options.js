export default {
  listItems: [
    {
      name: 'First Login',
      route: 'firstTimeLogin',
      icon: 'setting'
    },
    {
      name: 'Login',
      route: 'loginScreen',
      icon: 'setting'
    },
    {
      name: 'NotificationScreen',
      route: 'notificationScreen',
      icon: 'setting'
    },
    {
      name: 'MenuScreen',
      route: 'menuScreen',
      icon: 'setting'
    },
    {
      name: 'Customer Response',
      route: 'customerResponse',
      icon: 'setting'
    },
    {
      name: 'Danh sách giao dịch',
      route: 'transactionList',
      icon: 'setting'
    },
    {
      name: 'Chi tiết giao dịch',
      route: 'transactionDetail',
      icon: 'setting'
    },
    {
      name: 'Tạo chương trình bán hàng',
      route: 'createSaleProgram',
      icon: 'setting'
    },
    {
      name: 'Fake',
      route: 'fake',
      icon: 'setting'
    },
    {
      name: 'Network',
      route: 'network',
      icon: 'network',
    },
    {
      name: 'Activity Log',
      route: 'activityLog',
      icon: 'activity',
    },
    {
      name: 'Account Setting',
      route: 'user/setting',
      icon: 'setting',
    },
    {
      name: 'Manage Interaction',
      route: 'searchbar',
      icon: 'interaction',
    },
    {
      name: 'Calendar',
      route: 'spinner',
      icon: 'calendar',
    },
    {
      name: 'Reminder',
      route: 'tab',
      icon: 'reminder',
    },
    {
      name: 'Poll',
      route: 'tabs',
      icon: 'poll',
    },
    {
      name: 'Help & Support',
      route: 'thumbnail',
      icon: 'help',
    },

  ]

}
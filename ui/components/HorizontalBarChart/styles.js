import {CHART_VALUE_COLOR, CHART_LABEL_COLOR, PRIMARY_COLOR} from '~/store/constants/colors'
export default {
    chartContainer: {
        width: '100%',
        padding: 10,
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    chartRow: {
        flexDirection: 'row',
        marginBottom: 15
    },
    label: {
        width: 100,
        marginRight: 20,
        color: CHART_LABEL_COLOR
    },
    barContainer: {
        flexDirection: 'column',
    },
    value: {
        color: CHART_VALUE_COLOR,
    },
    bar:{
        width: 200,
        height: 7,
        backgroundColor: PRIMARY_COLOR
    }

}
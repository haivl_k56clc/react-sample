import React, { Component } from 'react'
import { connect } from 'react-redux'
import { List, ListItem, Text, Icon, Thumbnail, Button } from 'native-base'
import { View, TouchableWithoutFeedback, Animated, Easing } from 'react-native'
import styles from './styles'
import Content from '~/ui/components/Content'


export default class HorizontalBarChart extends Component {
    constructor(props) {
        super(props)
    }
    calulateChart(chartData){
        var mapValues = chartData.map(value=>value.value);
        var max = Math.max(...mapValues)

        chartData.forEach((value, key)=>{
            value['width'] = Math.ceil(value.value/max *200)
        })
        return chartData
    }

    render() {
        // {...styles.bar, ...value.color}
        const {chartData} = this.props
        const calulatedChart = this.calulateChart(chartData)
        return (
            <View style={styles.chartContainer}>
                {calulatedChart.map((value, key) => {
                    return (
                        <View key={key} style={styles.chartRow}>
                            <Text style={styles.label}>{value.label}</Text>
                            <View style={styles.barContainer}>
                                <Text style={styles.value}>{parseInt(value.value).toLocaleString('vi-VN')}</Text>
                                <View style={{...styles.bar, backgroundColor: value.color, width: value.width}}></View>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }
}
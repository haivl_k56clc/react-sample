import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    List, ListItem, Text, Icon, Thumbnail, Button, Content,
    InputGroup, Input, Radio, CheckBox
} from 'native-base'
import { View, TouchableWithoutFeedback, Animated, Easing, TextInput, Image } from 'react-native'
import styles from './styles'
import PhotoChooser from '~/ui/components/PhotoChooser'
import DatePicker from '~/ui/components/DatePicker'
@connect(state=>({xsession: state.auth.xsession}))
export default class CreateSaleProgram extends Component {
    constructor(props) {
        super(props)
        this.state = {
            saleImage: { uri: "http://english.tw/wp-content/themes/qaengine/img/default-thumbnail.jpg" },
            selectedStartDate: undefined
        }
    }
    _handleChoosePhoto = ({ uri, data }) => {
        // update 'data:image/jpeg;base64,' + data
        this.setState({ saleImage: { uri: uri } })
    }
    componentDidMount(){
        console.log(this.props.xsession)
    }
    render() {
        const { saleImage } = this.state;
        return (
            <Content>
                <View style={styles.container}>
                    <View style={styles.imagePickerBlock}>
                        <Image source={saleImage} style={{ width: '100%', height: '100%' }} resizeMode='contain' />
                        <Icon name="keyboard-arrow-left" style={styles.leftArrow} />
                        <Icon name="keyboard-arrow-right" style={styles.rightArrow} />
                        <PhotoChooser onSuccess={this._handleChoosePhoto} style={styles.photoChooser}>
                            <Icon name="local-see" style={styles.cameraIcon}/>
                        </PhotoChooser>
                    </View>


                    <InputGroup regular style={styles.rowInput}>
                        <Input placeholder='Tiêu đề' />
                    </InputGroup>
                    <View style={styles.row}>
                        <InputGroup regular style={styles.rowItem}>
                            {/*<Input placeholder='Từ ngày' />*/}
                            <DatePicker
                                mode="date"
                                placeholder="Từ ngày"
                                date={this.state.selectedStartDate}
                                onDateChange={(date) => this.setState({ selectedStartDate: date })}
                                customStyles={{
                                    dateIcon: { color: 'dimgrey' },
                                    dateInput: { color: 'dimgrey'}
                                }}
                                format="YYYY-MM-DD"
                                displayFormat="DD/MM/YYYY"
                            >
                                <Icon name="event-note" style={styles.calendarIcon} />
                            </DatePicker>
                        </InputGroup>
                        <InputGroup regular style={styles.rowItem}>
                            {/*<Input placeholder='Đến ngày' />*/}
                            <DatePicker
                                mode="date"
                                date={this.state.selectedEndDate}
                                onDateChange={(date) => this.setState({ selectedEndDate: date })}
                                customStyles={{
                                    dateIcon: { color: 'dimgrey' },
                                    dateInput: { color: 'dimgrey'}
                                }}
                                placeholder="Đến ngày"
                                format="YYYY-MM-DD"
                                displayFormat="DD/MM/YYY"Y
                            >
                                <Icon name="event-note" style={styles.calendarIcon} />
                            </DatePicker>
                        </InputGroup>
                    </View>

                    <View style={styles.rowInput}>
                        <Text>Loại khuyến mãi: Giảm giá theo %</Text>
                        <Icon name="keyboard-arrow-right" />
                    </View>

                    <View style={styles.row}>
                        <View style={styles.rowItem}>
                            <Text>Giảm/Tặng</Text>
                        </View>
                        <View style={styles.rowItem}>
                            <Text>%</Text>
                            <Input key="numeric" keyboardType="numeric" style={styles.textInputStyle} />
                        </View>
                    </View>


                    <View style={styles.promotionTypeBlock}>
                        <View style={styles.row}>
                            <View style={styles.justRow}>
                                <Text>Khuyến mãi Cashback</Text>
                                <Icon name="attach-money" style={{ color: 'limegreen' }} />
                            </View>
                            <Radio selected={true} />
                        </View>
                        <View style={styles.justRow}>
                            <Text>Khuyến mãi thường</Text>
                            <Radio selected={false} />
                        </View>
                    </View>
                    <InputGroup regular style={styles.rowInput}>
                        <TextInput placeholder='Mô tả khuyến mãi' numberOfLines={5} multiline={true} style={styles.textInputStyle} />
                    </InputGroup>
                    <InputGroup regular style={styles.rowInput}>
                        <TextInput placeholder='Từ khóa tìm kiếm' style={styles.textInputStyle} />
                    </InputGroup>
                    <View style={styles.seperator}>
                        <Text style={styles.seperatorFakeDot}>. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.applyPlaceLabel}>Địa điểm áp dụng</Text>
                        <Text style={styles.markAllLabel}>Đánh dấu tất cả</Text>
                    </View>
                    <List>
                        <ListItem style={styles.listCheckbox}>
                            <Text>33 Nguyễn Chí Thanh, Ba Đình, HN</Text>
                            <CheckBox checked={true} />
                        </ListItem>
                        <ListItem style={styles.listCheckbox}>
                            <Text>98 Hoàng Quốc Việt, Cầu Giấy, HN</Text>
                            <CheckBox checked={false} />
                        </ListItem>
                    </List>
                    <Button style={styles.submitButton}>
                        <Text>Ok</Text>
                    </Button>
                </View>
            </Content>
        )
    }
}

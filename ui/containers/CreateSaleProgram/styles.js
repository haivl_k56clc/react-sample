import {PRIMARY_COLOR} from '~/store/constants/colors'
export default {
    container: {
        padding: 10,
        backgroundColor: 'white'
    },
    imagePickerBlock: {
        backgroundColor: 'lightgrey',
        height: 200
    },
    photoChooser: {
        position: 'absolute',
        right: 5,
        bottom: 5,
        // backgroundColor: 'transparent'
    },
    cameraIcon: {
        color: 'dimgrey',
    },
    leftArrow: {
        position: 'absolute',
        left: 3,
        top: '47%',
        color: 'dimgrey'
    },
    rightArrow:{
        position: 'absolute',
        right: 3,
        top: '47%',
        color: 'dimgrey'
    },
    row:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5
    },
    justRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rowSpaceBetween: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rowItem:{
        backgroundColor: 'lightgrey',
        borderRadius: 4,
        padding: 5,
        width: '48%',
        flexDirection: 'row',
        alignItems: 'center',
        height: 50
    },
    rowInput: {
        backgroundColor: 'lightgrey',
        borderRadius: 4,
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 50,
        alignItems: 'center',
        padding: 5
    },
    textInputStyle: {
        width: '100%',
        height: 50,
        fontSize: 17,
        lineHeight: 24,
        paddingLeft: 5,
        paddingRight: 5
    },
    seperator: {
        // borderStyle: 'dotted',
        // borderBottomWidth: 2, 
        // borderStyle: 'dotted',
        // borderBottomColor: 'black',
        marginBottom: 5,
        marginTop: 5,
        // borderRadius: 1
    },
    applyPlaceLabel: {
        fontWeight: 'bold'
    },
    markAllLabel: {
        color: PRIMARY_COLOR
    },
    listNone: {
        borderBottomColor: 'transparent'
    },
    listCheckbox: {
        borderBottomColor: 'transparent',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        marginLeft: 0,
        paddingRight: 0
    },
    seperatorFakeDot: {
        color: 'grey',
        alignSelf: 'center'
    },
    submitButton:{
        width: '100%',
        justifyContent: 'center'
    },
    calendarIcon: {
        color: 'dimgrey'
    }
}
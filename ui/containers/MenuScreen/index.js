import React, { Component } from 'react'
import { connect } from 'react-redux'
import {List, ListItem, Text, Icon, Thumbnail} from 'native-base'
import {TouchableHighlight} from 'react-native'
import {View} from 'react-native'
import styles from './styles'
import {NOTIFICATION_TYPE} from '~/store/constants/app'
import * as notificationAction from '~/store/actions/notification'
import menu from '~/store/constants/menu'
export default class MenuScreen extends Component {
    _handlePress(item){
    }
    render() {
        return (
            <List style={styles.menuList} dataArray={menu}
                renderRow={(item) =>
                    <ListItem style={styles.listItem}>
                        <TouchableHighlight onPress={()=>this._handlePress(item)}>
                            <Text>{item.name}</Text>
                        </TouchableHighlight>
                    </ListItem>
                }>
            </List>
        )
    }
}
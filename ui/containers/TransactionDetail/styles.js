import {PRIMARY_COLOR} from '~/store/constants/colors'
export default {
    container:{
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    transactionBlock:{
        backgroundColor: 'lavender',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        
    },
    transactionContent:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    transactionLabel:{

    },
    transactionID: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 14
    },
    transactionIcon: {
        color: 'limegreen',
    },
    userBlock: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    userLabel:{

    },
    userContent: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    username:{
        fontWeight: 'bold'
    },
    avatar: {
        width: 40,
        height: 40
    },
    statusBlock: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    mark: {

    },
    statusTextWaiting: {
        fontWeight: 'bold',
        color: 'orange'
    },

    bar: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    barPart: {
        flexDirection: 'column',
        width: '33.33%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    barOne:{
        backgroundColor: 'blue',
        width: '100%',
        height: 5,
    },
    arrowDown:{
        width: 0,
        height: 0,
        borderWidth: 5,
        borderTopColor: 'blue',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        alignSelf: 'center',
        bottom: -5
    },
    barTwo:{
        backgroundColor: 'lightblue',
        width: '100%',
        height: 5
    },
    arrowUp: {
        width: 0,
        height: 0,
        borderWidth: 5,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'lightblue',
        alignSelf: 'center',
        bottom:5
    },
    barThree: {
        backgroundColor: 'orange',
        width: '100%',
        height: 5
    },
    barHiddenPartTop: {
        width: 0,
        height: 0,
        borderWidth: 5,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        alignSelf: 'center',
        bottom:-5
    },
     barHiddenPartBottom: {
        width: 0,
        height: 0,
        borderWidth: 5,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        alignSelf: 'center',
        bottom:5
    },
    invoiceStatusBlock: {
        padding: 10,
        justifyContent: 'flex-end'
    },
    invoiceStatusText: {
        alignSelf: 'flex-end'
    },
    paymenMethodBlock: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    paymenMethodLabel: {

    },
    paymenMethodContent: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    paymenMethodIcon: {

    },
    paymenMethod: {
        fontWeight: 'bold'
    },
    borderBlock: {
        borderWidth: 1,
        margin: 10,
        padding: 10,
        borderColor: PRIMARY_COLOR,
        flexDirection: 'column',
        // height: 300
    },

    invoiceBlock: {
        flexDirection: 'row',
        justifyContent: 'center',
        bottom: -20,
        zIndex: 20
    },
    invoiceLabel: {
        backgroundColor: 'white',
        paddingLeft: 5
    },
    invoice: {
        backgroundColor: 'white',
        fontWeight: 'bold',
        color: 'blue',
        paddingRight: 5
    },

    invoiceDetailBlock: {
        flexDirection: 'column',
        // flexWrap: 'wrap'
    },
    row: {
        flexDirection: 'row'
    },

    invoiceMoneyBlock: {
        justifyContent: 'center',
        width: '50%',
        padding: 5
    },
    invoiceMoney:{
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 28
    },
    invoiceMoneyLabel: {
        alignSelf: 'center',
        color: 'grey'
    },
    
    discountBlock: {
        justifyContent: 'center',
        width: '50%',
        padding: 5
    },
    discount: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 28,
        color: 'orange'
    },
    discountLabel: {
        alignSelf: 'center',
        color: 'grey'
    },
    
    cashbackBlock: {
        justifyContent: 'center',
        width: '50%',
        padding: 5
    },
    cashback: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 28,
        color: 'limegreen'
    },
    cashbackLabel: {
        alignSelf: 'center',
        color: 'grey',
    },

    clingmeFeeBlock: {
        justifyContent: 'center',
        width: '50%',
        padding: 5
    },
    clingmeFee: {
        alignSelf: 'center',
        color: 'blue',
        fontSize: 28,
        fontWeight: 'bold'
    },
    clingmeFeeLabel: {
        alignSelf: 'center',
        color: 'grey'
    },

    deliverBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    deliverIDLabel: {},
    deliverID:{
        fontWeight: 'bold'
    },

    preOrderBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    preOrderIDLabel: {},
    preOrderID: {
        fontWeight: 'bold'
    },
    confirmBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
    },
    confirmButton: {
        width: '45%',
        justifyContent: 'center'
    },
    navigateInvoiceBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    previousInvoiceBlock: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    nextInvoiceBlock: {
        flexDirection: 'row',
        alignItems: 'center'
    }

}
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { List, ListItem, Text, Icon, Thumbnail, Button, Content } from 'native-base'
import { View, TouchableWithoutFeedback, Animated, Easing } from 'react-native'
import styles from './styles'
export default class TranstionDetail extends Component {
    render() {
        return (
            <Content>
                <View style={styles.container}>
                    <View style={styles.transactionBlock}>
                        <View style={styles.transactionContent}>
                            <Text style={styles.transactionLabel}>Số giao dịch: </Text>
                            <Text style={styles.transactionID}>#CL12345</Text>
                        </View>
                        <Icon name="attach-money" style={styles.transactionIcon} />
                    </View>
                    <View style={styles.userBlock}>
                        <Text style={styles.userLabel}>Khách hàng:</Text>
                        <View style={styles.userContent}>
                            <Text style={styles.username}>Username</Text>
                            <Thumbnail source={{ uri: 'http://mobi.clingme.vn:8090/images/resource_image/Clingme_icon_512.png' }} style={styles.avatar} />
                        </View>
                    </View>
                    <View style={styles.statusBlock}>
                        <Text style={styles.mark}>Đánh dấu: 00:00:00 10/10/2010</Text>
                        <Text style={styles.statusTextWaiting}>Chờ xử lí</Text>
                    </View>

                    <View style={styles.bar}>
                        <View style={styles.barPart}>
                            <View style={styles.arrowDown}></View>
                            <View style={styles.barOne}></View>
                            <View style={styles.barHiddenPartBottom}></View>
                        </View>
                        <View style={styles.barPart}>
                            <View style={styles.barHiddenPartTop}></View>
                            <View style={styles.barTwo}></View>
                            <View style={styles.arrowUp}></View>
                        </View>
                        <View style={styles.barPart}>
                            <View style={styles.barHiddenPartTop}></View>
                            <View style={styles.barThree}></View>
                            <View style={styles.barHiddenPartBottom}></View>
                        </View>
                    </View>


                    <View style={styles.invoiceStatusBlock}>
                        <Text style={styles.invoiceStatusText}>Xuất hóa đơn: 00:00:00 11/11/2011</Text>
                    </View>
                    <View style={styles.paymenMethodBlock}>
                        <Text style={styles.paymenMethodLabel}>Hình thức thanh toán:</Text>
                        <View style={styles.paymenMethodContent}>
                            <Icon name="payment" style={styles.paymenMethodIcon} />
                            <Text style={styles.paymenMethod}>Thanh toán trực tiếp</Text>
                        </View>
                    </View>
                    <View style={styles.invoiceBlock}>
                        <Text style={styles.invoiceLabel}>Số hóa đơn: </Text>
                        <Text style={styles.invoice}>000425</Text>
                    </View>
                    <View style={styles.borderBlock}>
                        <View style={styles.invoiceDetailBlock}>
                            <View style={styles.row}>
                                <View style={styles.invoiceMoneyBlock}>
                                    <Text style={styles.invoiceMoney}>666.666đ</Text>
                                    <Text style={styles.invoiceMoneyLabel}>Tổng tiền hóa đơn</Text>
                                </View>
                                <View style={styles.discountBlock}>
                                    <Text style={styles.discount}>-20%</Text>
                                    <Text style={styles.discountLabel}>Tỷ lệ giảm giá</Text>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.cashbackBlock}>
                                    <Text style={styles.cashback}>69.699đ</Text>
                                    <Text style={styles.cashbackLabel}>Tổng tiền Cashback</Text>
                                </View>
                                <View style={styles.clingmeFeeBlock}>
                                    <Text style={styles.clingmeFee}>10.111đ</Text>
                                    <Text style={styles.clingmeFeeLabel}>Phí Clingme</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ height: 200, backgroundColor: 'lightgrey' }}></View>
                        <View style={styles.deliverBlock}>
                            <Text style={styles.deliverIDLabel}>Mã giao hàng:</Text>
                            <Text style={styles.deliverID}>0</Text>
                        </View>
                        <View style={styles.preOrderBlock}>
                            <Text style={styles.preOrderIDLabel}>Mã đặt trước:</Text>
                            <Text style={styles.preOrderID}>#FF6969</Text>
                        </View>
                    </View>
                    <View style={styles.confirmBlock}>
                        <Button light style={styles.confirmButton}>
                            <Text style={styles.confirmButtonText}>Không đồng ý</Text>
                        </Button>
                        <Button primary style={styles.confirmButton}>
                            <Text style={styles.confirmButtonText}>Đồng ý</Text>
                        </Button>
                    </View>
                    <View style={styles.navigateInvoiceBlock}>
                        <View style={styles.previousInvoiceBlock}>
                            <Icon name="keyboard-arrow-left" />
                            <Text>Giao dịch trước</Text>
                        </View>
                        <View style={styles.nextInvoiceBlock}>
                            <Text>Giao dịch sau</Text>
                            <Icon name="keyboard-arrow-right" />
                        </View>
                    </View>
                </View>
            </Content>
        )
    }
}
export default styles={
    notificationList: {
        backgroundColor: 'white'
    },
    leftThumbnail:{

    },
    rightIcon: {
    },
    middleContent: {
        justifyContent: 'flex-start',
        flexDirection: 'column',
        width: '75%'
    },
    notificationItem: {
        flexDirection: 'row'
    },
    headerMessage:{
        alignSelf: 'flex-start'
    },
    bodyMessage:{
        alignSelf: 'flex-start'
    },
    date: {
        alignSelf: 'flex-start'
    },
}
import React, { Component } from 'react'
import { connect } from 'react-redux'
import {List, ListItem, Text, Icon, Thumbnail} from 'native-base'
import {View} from 'react-native'
import styles from './styles'
import {NOTIFICATION_TYPE} from '~/store/constants/app'
import Content from '~/ui/components/Content'
import * as notificationAction from '~/store/actions/notification'
import * as commonActions from '~/store/actions/common'
@connect(state=>({  
    notifications: state.notification,
    getNotificationRequest: state.requests.getNotification,
    xsession: state.auth.userClingme.xsession
}), {...notificationAction,...commonActions})
export default class NoficationScreen extends Component {
    _handlePress(item){
        this.props.forwardTo(`notification/${item.user}`)
    }
    constructor(props){
        super(props)
       
    }
    componentWillMount(){
        this.props.clearNotificationState()
        this.props.getNotificationFromApi(1, this.props.xsession);
    }
    componentDidMount(){
    
    }
    _renderItem(item){
        switch(item.type){
            case NOTIFICATION_TYPE.COMMENT:
                return {
                    headerMessage: `${item.user} đã bình luận ${item.placeName}`,
                    bodyMessage: `Tại: ${item.place}`,
                    icon: "comment",
                    iconStyle: {color:'orange'},
                }
            case NOTIFICATION_TYPE.REPLY_COMMENT:
                 return {
                    headerMessage: `${item.user} đã phản hồi bình luận của ${item.author}`,
                    bodyMessage: `Tại: ${item.place}`,
                    icon: "comment",
                    iconStyle: {color: 'orange'}
                }
            case NOTIFICATION_TYPE.TRANSACTION_ONLINE:
                return {
                    headerMessage: `${item.user} có giao dịch thành công`,
                    bodyMessage: `Tại: ${item.place}`,
                    icon: "shopping-cart",
                    iconStyle: {color: 'green'}
                }
            case NOTIFICATION_TYPE.TRANSACTION_OFFLINE:
                return {
                    headerMessage: `${item.user} có giao dịch chờ xử lí`,
                    bodyMessage: `Tại: ${item.place}`,
                    icon: "shopping-cart",
                    iconStyle: {color: 'green'}
                }
            case NOTIFICATION_TYPE.REMINDER_EXPIRED:
                return {
                    headerMessage: `${item.placeName} đã hết hạn ${item.expireInterval}`,
                    bodyMessage: `Tại ${item.place}`,
                    icon: "warning",
                    iconStyle: {color: 'red'}
                }
            case NOTIFICATION_TYPE.REMINDER_WILL_EXPIRED:
                return {
                    headerMessage: `${item.placeName} còn ${item.expireInterval}`,
                    bodyMessage: `Tại ${item.place}`,
                    icon: "warning",
                    iconStyle: {color: 'red'}
                }
            case NOTIFICATION_TYPE.SYSTEM_SUPPORT:
                return {
                    headerMessage: "Thông báo từ Clingme",
                    bodyMessage: item.content,
                    icon: "mail",
                    iconStyle: {color: "orange"}
                }
            case NOTIFICATION_TYPE.SYSTEM_DEBT:
                return {
                    headerMessage: "Thông báo từ Clingme",
                    bodyMessage: item.content,
                    icon: "mail-outline",
                    iconStyle: {color: "orange"}
                }
            default:
                return {
                    headerMessage: "Template",
                    bodyMessage: "Template",
                    icon: "location-on",
                    iconStyle: {color: "black"}
                }               
        }
    }
    _renderItem2(item){
        switch(item.notifyType){
            case 1:
            case 2:
            case 3:
            case 4:
            default:
            
        }
    }
    _formatDate(timeStampSecond){
        let date = new Date(timeStampSecond*1000)
        return date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()
    }
    render() {
        const {notifications,getNotificationRequest,getNotification} = this.props
        // customStyle = {bold:NOTIFICATION_TYPE.SYSTEM_SUPPORT|| item.type==NOTIFICATION_TYPE.SYST_DEBT}
        // {...customStyle}
        /*return (
            <List style={styles.notificationList} dataArray={notifications}
                renderRow={(item) =>
                    <ListItem onPress={e=>this._handlePress(item)}>
                        <View style={styles.notificationItem}>
                            <Thumbnail source={{uri:'http://mobi.clingme.vn:8090/images/resource_image/Clingme_icon_512.png'}} style={styles.leftThumbnail}/>
                            <View style={styles.middleContent}>
                                <Text style={styles.headerMessage}>{(this._renderItem(item).headerMessage)}</Text>
                                <Text style={styles.bodyMessage}>{(this._renderItem(item).bodyMessage)}</Text>
                                <Text style={styles.date}>{item.date}</Text>
                            </View>
                            <Icon 
                                name={(this._renderItem(item)).icon} 
                                style={(this._renderItem(item)).iconStyle}/>
                        </View>
                    </ListItem>
                }>
            </List>
        )*/
        return (
            <List style={styles.notificationList} dataArray={notifications}
                renderRow={(item) =>
                    <ListItem onPress={e=>this._handlePress(item)} key={item.bizAccountId}>
                        <View style={styles.notificationItem}>
                            <Thumbnail source={{uri:item.icon}} style={styles.leftThumbnail}/>
                            <View style={styles.middleContent}>
                                <Text style={styles.headerMessage}>{item.title}</Text>
                                <Text style={styles.bodyMessage}>{item.content}</Text>
                                <Text style={styles.date}>{this._formatDate(item.createdTime)}</Text>
                            </View>
                            <Thumbnail source={{uri:item.icon}} style={styles.leftThumbnail}/>
                            {/*<Icon 
                                name={(this._renderItem(item)).icon} 
                                style={(this._renderItem(item)).iconStyle}/>*/}
                        </View>
                    </ListItem>
                }>
            </List>
        )
    }
}
import React, { Component } from 'react'
import {Container,Text} from 'native-base'
export default class extends Component {

    render(){
        const {route} = this.props
        return (
            <Container>
            <Text>{route.params.user}</Text>
            </Container>
        )
    }
}
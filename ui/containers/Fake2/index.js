import React, { Component } from 'react';
import { Container, Content, Text, Button } from 'native-base';
import { connect } from 'react-redux'
import * as commonActions from '~/store/actions/common'
@connect(null, commonActions)
export default class extends Component {
    render() {
        return (
            <Container>
                <Content>
                    <Text>This is fake2!</Text>
                    <Button onPress={()=>this.props.forwardTo('home')}>
                        <Text>Back To Home</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
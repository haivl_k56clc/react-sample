export default {
    container: {
        backgroundColor: '#21a8e2',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    noticeContainer: {
        height: 100,
        alignItems: 'center'
    },
    notice: {
        color: 'white',
    },
    input: {
        backgroundColor: 'white',
        width: '80%',
        padding: 5,
        borderRadius: 3,
        marginBottom: 10,
        height: 50,
        alignItems: 'center',
        flex: 0
    },
    updateButton: {
        width: '80%',
        justifyContent: 'center',
        backgroundColor: '#197CAF',
        alignSelf: 'center',
        elevation: 0
    }
}
import React, { Component } from 'react'
import styles from './styles'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import {View} from 'react-native'
import {Text, Input, Button, Item} from 'native-base'
export default class LoginScreen extends Component{
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.noticeContainer}>
                    <Text style={styles.notice}>
                        Đây là lần đầu đăng nhập của bạn
                    </Text>
                    <Text style={styles.notice}>
                        Vui lòng tạo Mật khẩu riêng để bảo mật
                    </Text>
                </View>
                <Input placeholder="Mật khẩu hiện tại" style={styles.input} secureTextEntry={true}/>
                <Input placeholder="Mật khẩu mới" style={styles.input} secureTextEntry={true}/>
                <Input placeholder="Nhập lại mật khẩu mới" style={styles.input} secureTextEntry={true}/>
                <Button style={styles.updateButton}><Text>Cập nhật</Text></Button>
            </View>
        )
    }
}
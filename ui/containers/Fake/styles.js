export default {
    container:{
        justifyContent: 'center',
        // alignItems: 'center', 
        // alignContent: 'center',
        flexDirection: 'column',      
        backgroundColor: 'white'
    },
    button:{
        alignSelf:'center',
        marginTop: 5,
        marginBottom: 5
    },
    subContainer:{
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
    },
    buttonTab:{
        margin: 2,
    },
    buttonTab3:{
        margin: 2,
        flexGrow: 2,
        justifyContent: 'center'
    },
    subContainer2:{
        
    },
    test: {
    },
    signup: {
        marginTop: 5,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    myFab: {    
        position: 'absolute',
        bottom: 65,
        right: 5,
        zIndex: 10000
    },
    listContainer: {
        marginBottom: 200
    },
    loadingContainer: {
        marginTop: 50,
    },
    myTabs:{
        backgroundColor: 'white',
        
    }

}
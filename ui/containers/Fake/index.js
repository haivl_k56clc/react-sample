import React, { Component } from 'react'
import {
    Container, Content, Card,
    Tabs, Tab, TabHeading, ScrollableTab,
    CardItem, Text, Body, Button, Spinner,
    Icon, Form, Input, Label, Item, Toast,
    Fab, Footer, FooterTab, Badge, List,
    ListItem
} from 'native-base'
import { View, InteractionManager } from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import * as commonActions from '~/store/actions/common'
@connect(null, commonActions)
export default class extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false,
            tab: {
                tab1: true,
                tab2: false,
                tab3: false,
                tab4: false
            },
            renderPlaceHolderOnly: true
        }
    }
    _renderPlaceholderView() {
        return (
            <View style={styles.loadingContainer}>
                <Text>Loading...</Text>
            </View>
        )
    }
    componentDidMount() {
        this.setState({ renderPlaceholderOnly: false });

        // InteractionManager.runAfterInteractions(() => {
        //     this.setState({renderPlaceholderOnly: false});
        // });
    }
    render() {
        // if (this.state.renderPlaceHolderOnly){
        //     return this._renderPlaceholderView();
        // }
        const { forwardTo } = this.props
        const { tab1 } = this.state.tab
        var items = [
            {
                isHeader: true,
                text: 'Header1'
            },
            {
                isHeader: false,
                text: 'Simon Mignolet',
            },
            {
                isHeader: true,
                text: 'Header2'
            },
            {
                isHeader: false,
                text: 'Nathaniel Clyne',
            },
            {
                isHeader: false,
                text: 'Nathaniel Clyne'
            },
            {
                isHeader: false,
                text: 'Dejan Lovren',
            },
            {
                isHeader: false,
                text: 'Mama Sakho',
            },
            {
                isHeader: false,
                text: 'Emre Can',
                isLast: true
            },
        ];

        return (

            <Container style={styles.container}>
                <Button rounded dark style={styles.myFab}>
                    <Text>FAB</Text>
                </Button>

                <Tabs style={styles.myTabs}>
                    <Tab heading={<TabHeading><Icon name="camera" /><Text>Camera</Text></TabHeading>}>
                    </Tab>
                    <Tab heading={<TabHeading><Text>No Icon</Text></TabHeading>}>
                    </Tab>
                    <Tab heading={<TabHeading><Icon name="apps" /></TabHeading>}>
                    </Tab>
                </Tabs>


                <Content>
                    <Text>
                        This is a fake page Haha :))!
                    </Text>
                    <Button style={styles.button} success onPress={e => forwardTo('fake2')}>
                        <Text>Go to Fake2</Text>
                    </Button>
                    <Button style={styles.button} primary onPress={(e) => forwardTo('home')}>
                        <Text>Back To Home</Text>
                    </Button>
                    <View style={styles.subContainer}>
                        <Button light style={styles.buttonTab}>
                            <Text>Bt1</Text>
                        </Button>
                        <Button primary style={styles.buttonTab}>
                            <Text>Bt2</Text>
                        </Button>
                        <Button success style={styles.buttonTab3}>
                            <Text>Bt3</Text>
                        </Button>
                        <Button danger style={styles.buttonTab}>
                            <Text>Bt4</Text>
                        </Button>
                        <Button dark style={styles.buttonTab}>
                            <Text>Bt5</Text>
                        </Button>
                    </View>
                    <View style={styles.subContainer2}>
                        <Spinner color='blue' />
                    </View>
                    <View>
                        <Form>
                            <Item floatingLabel>
                                <Label>Username</Label>
                                <Input />
                            </Item>
                            <Item floatingLabel last>
                                <Label>Password</Label>
                                <Input />
                            </Item>
                            <Button style={styles.signup}>
                                <Text>Sign Up</Text>
                            </Button>
                        </Form>
                    </View>
                    <View style={styles.listContainer}>
                        <List>
                            <ListItem itemDivider>
                                <Text>A</Text>
                            </ListItem>
                            <ListItem >
                                <Text>Aaron Bennet</Text>
                            </ListItem>
                            <ListItem>
                                <Text>Ali Connors</Text>
                            </ListItem>
                            <ListItem itemDivider>
                                <Text>B</Text>
                            </ListItem>
                            <ListItem>
                                <Text>Bradley Horowitz</Text>
                            </ListItem>
                            <ListItem itemDivider>
                                <Text>C</Text>
                            </ListItem>
                            <ListItem >
                                <Text>Aaron Bennet</Text>
                            </ListItem>
                            <ListItem>
                                <Text>Ali Connors</Text>
                            </ListItem>
                        </List>
                    </View>
                    <View>
                        <List dataArray={items}
                            renderRow={(item) =>
                                <ListItem itemDivider={item.isHeader} last={item.isLast}>
                                    <Text>{item.text}</Text>
                                </ListItem>
                            }>
                        </List>
                    </View>
                </Content>
                <Footer >
                    <FooterTab>
                        <Button badge active={tab1} onPress={() => {
                            Toast.show({
                                text: 'Wrong password!',
                                position: 'bottom',
                                buttonText: 'Okay'
                            })
                            this.setState({ tab: { ...this.state.tab, tab1: !tab1 } })
                        }}>
                            <Badge><Text>2</Text></Badge>
                            <Icon name="apps" />
                            <Text>Apps</Text>
                        </Button>
                        <Button>
                            <Icon name="camera" />
                            <Text>Camera</Text>
                        </Button>
                        <Button active={this.state.tab.tab3} badge>
                            <Badge ><Text>51</Text></Badge>
                            <Icon active name="close" />
                            <Text>Navigate</Text>
                        </Button>
                        <Button>
                            <Icon name="person" />
                            <Text>Contact</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Content, List, ListItem, Text, Icon, Thumbnail } from 'native-base'
import { TouchableHighlight } from 'react-native'
import { View } from 'react-native'
import styles from './styles'
import { NOTIFICATION_TYPE } from '~/store/constants/app'
import * as notificationAction from '~/store/actions/notification'
@connect(state => ({
    notification: state.notification
}), notificationAction)
export default class CustomerResponse extends Component {
    _handlePress(item) {
        console.log('Item', item);
    }
    render() {
        var responses = [
            {
                storeId: 1,
                storeName: 'Nét Huế',
                address: '83 Trần Duy Hưng, Cầu Giấy, Hà Nội'

            },
            {
                storeId: 2,
                storeName: 'Nét Huế',
                address: '83 Trần Duy Hưng, Cầu Giấy, Hà Nội'
            },
            {
                storeId: 3,
                storeName: 'Nét Huế',
                address: '83 Trần Duy Hưng, Cầu Giấy, Hà Nội'
            }

        ]
        return (
            <Container>
                <Content>
                    <List style={styles.responseList} dataArray={responses}
                        renderRow={(item) =>
                            <ListItem>
                                <TouchableHighlight onPress={() => this._handlePress(item)}>
                                    <View>
                                        <Text>{item.storeName}</Text>
                                        <Text>{item.address}</Text>
                                    </View>
                                </TouchableHighlight>
                            </ListItem>
                        }>
                    </List>
                </Content>
            </Container>
        )
    }
}
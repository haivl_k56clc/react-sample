export default {

  roles: [
    {
      title: 'Normal',
      name: 'normal',
    },
    {
      title: 'Super',
      name: 'super',
    },
    {
      title: 'Custom',
      name: 'custom',
    },
  ],

  dateIndefinite: [
    {
      title: 'Indefinite',
      name: 'indefinite'
    },
    {
      title: 'Specify End date',
      name: 'endDateOption'
    },
  ],
       
  items: [
    {
      title: 'Basic',      
    },
    {
      title: 'Contact',
    },
    {
      title: 'Address',
    },
    {
      title: 'Financial',      
    },
    {
      title: 'Government ID',
    },
    {
      title: 'Education',      
    },
    {
      title: 'Employment',      
    },
    {
      title: 'Family',      
    },
    {
      title: 'Membership',      
    },
    {
      title: 'Others',      
    },
  ]
    
}
import React, { Component } from 'react'
import styles from './styles'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import {View} from 'react-native'
import {Text, Input, Button, Item} from 'native-base'
export default class FirstTimeLogin extends Component{
    render(){
        return (
            <View style={styles.loginContainer}>
                <Text style={styles.branchName}>Clingme</Text>
                <Input placeholder="Email/Số điện thoại" style={styles.input}/>
                <Input placeholder="Mật khẩu" style={styles.input} secureTextEntry={true}/>
                <Button style={styles.loginButton}><Text>Đăng nhập</Text></Button>
                <Text style={styles.forgotPassword}>Quên mật khẩu</Text>
            </View>
        )
    }
}
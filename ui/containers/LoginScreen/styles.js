export default {
    loginContainer: {
        backgroundColor: '#21a8e2',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    branchName: {
        fontSize: 48,
        color: 'white',
        height: 100
    },
    input: {
        backgroundColor: 'white',
        width: '80%',
        padding: 5,
        borderRadius: 3,
        marginBottom: 10,
        height: 50,
        alignItems: 'center',
        flex: 0
    },
    loginButton: {
        width: '80%',
        justifyContent: 'center',
        backgroundColor: '#197CAF',
        alignSelf: 'center',
        elevation: 0
    },
    forgotPassword: {
        color: 'white',
        marginTop: 20,
        fontWeight: '100'
    }
}
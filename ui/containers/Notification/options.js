export default {
  notifications:[  
    {
      icon: 'refresh',
      user: 'Bank ABC',
      message: 'has invited you to join a sync form.',
      time: '2 minutes ago'
    },
    {
      icon: 'refresh',
      user: 'Bank ABC',
      message: 'has requested to push information to your vault.',
      time: '10 minutes ago'
    },
    {
      icon: 'cloud-upload',
      user: 'Son Nguyen',
      message: 'has invited you to join a sync form.',
      time: '5 hours ago'
    },
    {
      icon: 'cloud-upload',
      user: 'Bank ABC',
      message: 'has requested to push information to your vault.',
      time: '5 hours ago'
    },
    {
      icon: 'network',
      user: 'Bank ABC',
      message: 'has invited you to join a sync form.',
      time: 'Yesterday at 23:49'
    },
    {
      icon: 'network',
      user: 'Vinh Long',
      message: 'has requested to push information to your vault.',
      time: 'Yesterday at 23:49'
    },
    {
      icon: 'delegation',
      user: 'Quay Do',
      message: 'has invited you to join a sync form.',
      time: '12 March at 23:49'
    },
    {
      icon: 'delegation',
      user: 'Bank ABC',
      message: 'has requested to push information to your vault.',
      time: '12 March at 23:49'
    },
    {
      icon: 'refresh',
      user: 'Bank ABC',
      message: 'has invited you to join a sync form.',
      time: '2 minutes ago'
    },
    {
      icon: 'refresh',
      user: 'Bank ABC',
      message: 'has requested to push information to your vault.',
      time: '10 minutes ago'
    },
  ]
}





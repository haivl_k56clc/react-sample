// it is like resource, not const, do not use constant style
export const profileCover = require('./profile-cover.png')
export const logo = require('./logo.png')
export const cardImage = require('./event-background.png')
export const regitIcon = require('./regit.png')